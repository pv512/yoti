package com.yoti;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JSonPayload {
	
	@SuppressWarnings("unchecked")
	public JSONObject getPayload(String payload) throws FileNotFoundException,
			IOException, ParseException {
		
		JSONParser parser = new JSONParser();
		String path = System.getProperty("user.dir");
		
		Object obj = parser.parse(new FileReader(path
				+ "/src/test/resources/json/permission.json"));
		
		JSONObject jsonPostBody = (JSONObject) obj;
		
		switch (payload) {

		case "no fields":
			jsonPostBody.remove("name");
			break;
		case "permission":
			jsonPostBody.put("domain", "https://my.domain.com");
			break;
		case "server error":
			jsonPostBody.remove("name");
			jsonPostBody.remove("domain");
			break;
		}
		return jsonPostBody;
	}

}
