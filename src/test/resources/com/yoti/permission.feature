Feature: Update Permission

  Scenario Outline: Put request for an application
    Given I construct json "<payload>"
    When I execute PUT request on "<URI>"
    Then I sees "<Status Code>"

    Examples: 
      | payload      | URI                   | Status Code |
      | valid        | http://apps/1         | 200         |
      | no fields    | http://apps/1         | 400         |
      | permission   | http://apps/1         | 304         |
      | wrong appID  | http://apps/something | 404         |
      | server error | http://apps/1         | 500         |
