package com.yoti;

import java.io.FileNotFoundException;
import java.io.IOException;
import static com.jayway.restassured.RestAssured.given;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import com.jayway.restassured.response.Response;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class PermissionSteps {
	JSONObject jsonBody;
	Response response;

	@Given("^I construct json \"(.*?)\"$")
	public void i_construct_json(String payload) throws FileNotFoundException,
			IOException, ParseException {
		this.jsonBody = new JSonPayload().getPayload(payload);

	}

	/**
	 * OAuth is used in order sample user name and password
	 * 
	 * @param URI
	 */
	@When("^I execute PUT request on \"(.*?)\"$")
	public void i_execute_PUT_request_on(String URI) {
		this.response = given().auth().basic("userName", "password")
				.body(jsonBody).when().put(URI);
	}

	@Then("^I sees \"(.*?)\"$")
	public void i_sees(int expectedCode) {
		response.then().statusCode(expectedCode);
	}
}
